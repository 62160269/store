/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hw.store.poc;

import database.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author BenZ
 */
public class UpdateProduct {
        public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "UPDATE product SET name = ?, price = ? WHERE id = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, "Latte");
            stmt.setDouble(2, 2000);
            stmt.setInt(3, 3);
            int row = stmt.executeUpdate();
            System.out.println("Affect now " + row);

        } catch (SQLException ex) {
            Logger.getLogger(SelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();

    }

    
}
